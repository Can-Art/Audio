﻿namespace AudioMicSpam
{
    partial class AudioMicSpam
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.cbSons = new System.Windows.Forms.ComboBox();
			this.btnPlay = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.cbPlayback = new System.Windows.Forms.CheckBox();
			this.bsSons = new System.Windows.Forms.BindingSource(this.components);
			this.btConfig = new System.Windows.Forms.Button();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.btStop = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.bsSons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.SuspendLayout();
			// 
			// cbSons
			// 
			this.cbSons.FormattingEnabled = true;
			this.cbSons.Location = new System.Drawing.Point(40, 40);
			this.cbSons.Name = "cbSons";
			this.cbSons.Size = new System.Drawing.Size(121, 21);
			this.cbSons.TabIndex = 3;
			// 
			// btnPlay
			// 
			this.btnPlay.Location = new System.Drawing.Point(167, 40);
			this.btnPlay.Name = "btnPlay";
			this.btnPlay.Size = new System.Drawing.Size(27, 23);
			this.btnPlay.TabIndex = 4;
			this.btnPlay.UseVisualStyleBackColor = true;
			this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(40, 84);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 5;
			this.btnAdd.Text = "Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(119, 84);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(75, 23);
			this.btnRemove.TabIndex = 6;
			this.btnRemove.Text = "Remove";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// cbPlayback
			// 
			this.cbPlayback.AutoSize = true;
			this.cbPlayback.Checked = true;
			this.cbPlayback.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbPlayback.Location = new System.Drawing.Point(216, 40);
			this.cbPlayback.Name = "cbPlayback";
			this.cbPlayback.Size = new System.Drawing.Size(70, 17);
			this.cbPlayback.TabIndex = 8;
			this.cbPlayback.Text = "Playback";
			this.cbPlayback.UseVisualStyleBackColor = true;
			// 
			// btConfig
			// 
			this.btConfig.Location = new System.Drawing.Point(40, 215);
			this.btConfig.Name = "btConfig";
			this.btConfig.Size = new System.Drawing.Size(75, 23);
			this.btConfig.TabIndex = 9;
			this.btConfig.Text = "Config";
			this.btConfig.UseVisualStyleBackColor = true;
			this.btConfig.Click += new System.EventHandler(this.btConfig_Click);
			// 
			// trackBar1
			// 
			this.trackBar1.Location = new System.Drawing.Point(228, 63);
			this.trackBar1.Maximum = 100;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trackBar1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.trackBar1.Size = new System.Drawing.Size(45, 173);
			this.trackBar1.SmallChange = 5;
			this.trackBar1.TabIndex = 10;
			this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.None;
			this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
			// 
			// btStop
			// 
			this.btStop.Location = new System.Drawing.Point(40, 124);
			this.btStop.Name = "btStop";
			this.btStop.Size = new System.Drawing.Size(154, 23);
			this.btStop.TabIndex = 11;
			this.btStop.Text = "Stop";
			this.btStop.UseVisualStyleBackColor = true;
			this.btStop.Click += new System.EventHandler(this.btStop_Click);
			// 
			// AudioMicSpam
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(343, 298);
			this.Controls.Add(this.btStop);
			this.Controls.Add(this.trackBar1);
			this.Controls.Add(this.btConfig);
			this.Controls.Add(this.cbPlayback);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.btnPlay);
			this.Controls.Add(this.cbSons);
			this.Name = "AudioMicSpam";
			this.Text = "AudioMicSpam";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.bsSons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbSons;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.BindingSource bsSons;
        private System.Windows.Forms.CheckBox cbPlayback;
        private System.Windows.Forms.Button btConfig;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button btStop;
    }
}

