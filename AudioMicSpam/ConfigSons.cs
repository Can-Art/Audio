﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using NAudio.Wave;

namespace AudioMicSpam
{
    public partial class ConfigSons : Form
    {
        private readonly Profil _leProfil;
        private int _waitForInput;

        public ConfigSons(Profil pLeProfil)
        {
            InitializeComponent();
            _leProfil = pLeProfil;
            _waitForInput = -1;
            dgvSons.AutoGenerateColumns = true;
        }

        private void ConfigSons_Load(object sender, EventArgs e)
        {
            bsSons.DataSource = _leProfil.LesSons;
            dgvSons.DataSource = bsSons;

            var laColumn = new DataGridViewButtonColumn();
            dgvSons.Columns.Add(laColumn);

            dgvSons.Refresh();

            cbIn.SelectedIndexChanged -= cbIn_SelectedIndexChanged;
            cbOut.SelectedIndexChanged -= cbOut_SelectedIndexChanged;
            cbOutVb.SelectedIndexChanged -= cbOutVb_SelectedIndexChanged;

            var lesDevicesIn = new List<string>();
            for (var i = 0; i < WaveIn.DeviceCount; i++)
            {
                lesDevicesIn.Add(WaveIn.GetCapabilities(i).ProductName);
            }
            cbIn.DataSource = lesDevicesIn;

            var lesDevicesOut = new List<string>();
            var lesDevicesOutVb = new List<string>();
            for (var i = 0; i < WaveOut.DeviceCount; i++)
            {
                lesDevicesOut.Add(WaveOut.GetCapabilities(i).ProductName);
                lesDevicesOutVb.Add(WaveOut.GetCapabilities(i).ProductName);
            }
            cbOut.DataSource = lesDevicesOut;
            cbOutVb.DataSource = lesDevicesOutVb;

            cbIn.SelectedIndex = _leProfil.In;
            cbOut.SelectedIndex = _leProfil.OutC;
            cbOutVb.SelectedIndex = _leProfil.OutVb;

            cbIn.SelectedIndexChanged += cbIn_SelectedIndexChanged;
            cbOut.SelectedIndexChanged += cbOut_SelectedIndexChanged;
            cbOutVb.SelectedIndexChanged += cbOutVb_SelectedIndexChanged;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("");
        }

        private void dgvSons_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 3) return;
            
            _waitForInput = e.RowIndex;
            lbInfo.Text = "appuyer sur une touche";
        }

        private void ConfigSons_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_waitForInput == -1) return;
            
             e.Handled = true;
             _waitForInput = -1;
             bsSons.ResetBindings(false);
             dgvSons.Refresh();
             lbInfo.Text = "";  
        }

        private void ConfigSons_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (_waitForInput == -1) return;

            _leProfil.LesSons[_waitForInput].Hotkey = e.KeyCode;
            //_leProfil.ModifSon(_leProfil.LesSons[_waitForInput]);
            Console.WriteLine("Changing bind");
        }

        private void cbOut_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOut.SelectedIndex == -1) return;

            _leProfil.OutC = cbOut.SelectedIndex;
        }

        private void cbIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbIn.SelectedIndex == -1) return;

            _leProfil.In = cbIn.SelectedIndex;
        }

        private void cbOutVb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOutVb.SelectedIndex == -1) return;

            _leProfil.OutVb = cbOutVb.SelectedIndex;
        }
    }
}
