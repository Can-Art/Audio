﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using Newtonsoft.Json;

namespace AudioMicSpam
{

    public partial class AudioMicSpam : Form
    {
        private WaveIn waveIn;
        private BufferedWaveProvider playBufferCasque;
        private BufferedWaveProvider playBufferVb;
        private BufferedWaveProvider playBufferMeter;

        private WaveOut waveOutCasqueSound;
        private WaveOut waveOutCasque;

        private WaveOut waveOutVbSound;
        private WaveOut waveOutVb;

        private WaveToSampleProvider converter;
        private MeteringSampleProvider sdq;

        public delegate void SetParameterValueDelegate(int value);
        public SetParameterValueDelegate SetParameterValueCallback;

        private readonly Profil _profil;

        public AudioMicSpam()
        {
            InitializeComponent();

            if (File.Exists(Application.StartupPath + @"/Profil.cfg"))
                using (var stream = new StreamReader(Application.StartupPath + @"/Profil.cfg"))
                    _profil = JsonConvert.DeserializeObject<Profil>(stream.ReadToEnd());
            else
                _profil = Profil.ProfilBase();

            cbPlayback.Checked = _profil.Playback;
            cbPlayback.CheckedChanged += cbPlayback_CheckedChanged;
			KeyHandler.SetUp(play, _profil.LesSons);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bsSons.DataSource = _profil.LesSons;
            cbSons.DataSource = bsSons;

            waveIn = new WaveIn(WaveCallbackInfo.FunctionCallback())
            {
	            BufferMilliseconds = 25,
	            DeviceNumber = _profil.In
            };
            waveIn.DataAvailable += _waveIn_DataAvailable;
            waveIn.WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(44100, 2);
			waveIn.StartRecording();

            playBufferCasque = new BufferedWaveProvider(WaveFormat.CreateIeeeFloatWaveFormat(44100, 2)) { DiscardOnBufferOverflow = true };

            playBufferMeter = new BufferedWaveProvider(WaveFormat.CreateIeeeFloatWaveFormat(44100, 2)) { DiscardOnBufferOverflow = true };

            converter = new WaveToSampleProvider(playBufferMeter);
            sdq = new MeteringSampleProvider(converter, 8000);
            sdq.StreamVolume += sdqOnStreamVolume;

            playBufferVb = new BufferedWaveProvider(WaveFormat.CreateIeeeFloatWaveFormat(44100, 2)) { DiscardOnBufferOverflow = true };

            waveOutCasque = new WaveOut(WaveCallbackInfo.FunctionCallback()) { DeviceNumber = _profil.OutC };
            waveOutCasque.Init(playBufferCasque);
            waveOutCasque.Play();

            waveOutVb = new WaveOut(WaveCallbackInfo.FunctionCallback()) { DeviceNumber = _profil.OutVb };
            waveOutVb.Init(playBufferVb);
            waveOutVb.Play();

			trackBar1.Value = _profil.DefaultVolume;

			_profil.Save();
        }

        private static void sdqOnStreamVolume(object sender, StreamVolumeEventArgs streamVolumeEventArgs)
        {
            if (float.IsNaN(streamVolumeEventArgs.MaxSampleValues[0])) return;

            if (streamVolumeEventArgs.MaxSampleValues[0] > 1)
            {
                Console.WriteLine($"nope {streamVolumeEventArgs.MaxSampleValues[0]}");
                return;
            }

            int value = Convert.ToInt32(streamVolumeEventArgs.MaxSampleValues[0] * 100);
            //if (value > 99) return;
            //Console.WriteLine(value);

            //Invoke(SetParameterValueCallback, value);
        }

        private void _waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            var tmpBuffer = new float[e.BytesRecorded];
            Buffer.BlockCopy(e.Buffer, 0, tmpBuffer, 0, e.BytesRecorded);
            sdq.Read(tmpBuffer, 0, e.BytesRecorded);

            if (cbPlayback.Checked) playBufferCasque.AddSamples(e.Buffer, 0, e.BytesRecorded);

            playBufferVb.AddSamples(e.Buffer, 0, e.BytesRecorded);
            playBufferMeter.AddSamples(e.Buffer, 0, e.BytesRecorded);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var fileBrowser = new OpenFileDialog
            {
                InitialDirectory = Application.StartupPath,
                Multiselect = true,
                Filter = "Audio|*.mp3;*.wav"
            };

            var result = fileBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {

                foreach (string fileName in fileBrowser.FileNames)
                {
                    if (_profil.LesSons.Count(x => x.Path == fileName) > 0) continue;

                    var leSon = new Son(fileName);
                    Console.WriteLine(fileName);
                    _profil.LesSons.Add(leSon);
                }
            }

            bsSons.ResetBindings(false);
            _profil.Save();
        }

        private void btnPlay_Click(object sender, EventArgs e) => play((Son)cbSons.SelectedItem);

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) => stopEncoding();

        private WaveStream wfrCasque;
        private WaveStream wfrVb;
        private readonly List<(WaveOut,WaveOut)> _lesSonsPlayed = new List<(WaveOut, WaveOut)>();

        private void onPlaybackStopped(object sender, StoppedEventArgs stoppedEventArgs)
        {
	        if (!(sender is WaveOut waveOut)) return;
			waveOut.Stop();
			waveOut.Dispose();
			_lesSonsPlayed.RemoveAll(x => x.Item1.PlaybackState == PlaybackState.Stopped);
        }

        private void play(Son pSon)
        {
            if (!File.Exists(pSon.Path))
            {
                remove(pSon);
                return;
            }
            
            waveOutCasqueSound = new WaveOut(WaveCallbackInfo.FunctionCallback())
            {
                DeviceNumber = waveOutCasque.DeviceNumber,
                Volume = waveOutCasque.Volume,
            };

            waveOutVbSound = new WaveOut(WaveCallbackInfo.FunctionCallback())
            {
                DeviceNumber = waveOutVb.DeviceNumber,
                Volume = waveOutCasque.Volume
            };

            waveOutCasqueSound.PlaybackStopped += onPlaybackStopped;
			waveOutVbSound.PlaybackStopped += onPlaybackStopped;

            _lesSonsPlayed.Add((waveOutCasqueSound, waveOutVbSound));

            if (Path.GetExtension(pSon.Nom) == ".mp3")
            {
                if (cbPlayback.Checked)
                    wfrCasque = new Mp3FileReader(pSon.Path);

                wfrVb = new Mp3FileReader(pSon.Path);
            }
            else
            {
                if (cbPlayback.Checked)
                    wfrCasque = new WaveFileReader(pSon.Path);

                wfrVb = new WaveFileReader(pSon.Path);
            }

			try
            {
	            waveOutVbSound.Init(wfrVb);
				waveOutVbSound.Play();
	            if (cbPlayback.Checked)
	            {
		            waveOutCasqueSound.Init(wfrCasque);
					waveOutCasqueSound.Play();
	            }
            }
            catch (Exception)
            {
	            waveOutVbSound.Dispose();
			}
        }

        private void stopEncoding()
        {
            _profil.Save();
            try
            {
                waveIn.StopRecording();
                waveIn.Dispose();
                waveIn = null;

                if (waveOutCasqueSound != null)
                {
                    waveOutCasqueSound.Stop();
                    waveOutCasqueSound.Dispose();
                    waveOutCasqueSound = null;
                    waveOutVbSound.Stop();
                    waveOutVbSound.Dispose();
                    waveOutVbSound = null;
                }

                waveOutCasque.Stop();
                waveOutCasque.Dispose();
                waveOutCasque = null;


                waveOutVb.Stop();
                waveOutVb.Dispose();
                waveOutVb = null;

                playBufferCasque = null;
                playBufferVb = null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + " " + e.InnerException);
            }
            KeyHandler.Dispose();
		}

        private void btnRemove_Click(object sender, EventArgs e) => remove((Son)cbSons.SelectedValue);

		private void remove(Son leSon)
        {
            _profil.LesSons.Remove(leSon);

            bsSons.ResetBindings(false);
            _profil.Save();
        }

        private void btConfig_Click(object sender, EventArgs e)
        {
            var leForm = new ConfigSons(_profil);
            leForm.ShowDialog();

            if (waveIn.DeviceNumber != _profil.In)
            {
                waveIn.StopRecording();
                waveIn = new WaveIn(WaveCallbackInfo.FunctionCallback())
                {
                    BufferMilliseconds = 25,
                    DeviceNumber = _profil.In
                };
                waveIn.DataAvailable += _waveIn_DataAvailable;
                waveIn.WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(44100, 2);
                waveIn.StartRecording();
            }

            if (waveOutCasque.DeviceNumber != _profil.OutC)
            {
                waveOutCasque.Stop();
                waveOutCasque = new WaveOut(WaveCallbackInfo.FunctionCallback()) { DeviceNumber = _profil.OutC };
                waveOutCasque.Init(playBufferCasque);
                waveOutCasque.Play();
            }

            if (waveOutVb.DeviceNumber != _profil.OutVb)
            {
                waveOutVb.Stop();
                waveOutVb = new WaveOut(WaveCallbackInfo.FunctionCallback()) { DeviceNumber = _profil.OutVb };
                waveOutVb.Init(playBufferVb);
                waveOutVb.Play();
            }

            _profil.Save();
        }

        private void cbPlayback_CheckedChanged(object sender, EventArgs e) => _profil.Playback = !_profil.Playback;

		private void trackBar1_Scroll(object sender, EventArgs e)
        {
            _profil.DefaultVolume = trackBar1.Value;

            waveOutCasque.Volume = trackBar1.Value / 100.0F;
            waveOutVb.Volume = trackBar1.Value / 100.0F; //baisse son voix ?
            if (waveOutCasqueSound == null) return;

            //_waveOutCasqueSound.Volume = trackBar1.Value / 100.0F;
            //_waveOutVbSound.Volume = trackBar1.Value / 100.0F; Lien déja fais dans le constructeur
        }

        private void btStop_Click(object sender, EventArgs e)
        {
            if (_lesSonsPlayed.Count == 0) return;

            for (int i = 0; i < _lesSonsPlayed.Count; i++)
            {
                _lesSonsPlayed[i].Item1.Stop();
                _lesSonsPlayed[i].Item1.Dispose();
                _lesSonsPlayed[i].Item2.Stop();
                _lesSonsPlayed[i].Item2.Dispose(); 
            }

            _lesSonsPlayed.Clear();
        }
    }
}
