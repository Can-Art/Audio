﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace AudioMicSpam
{
    public class Profil
    {
        public bool Playback;
        public List<Son> LesSons;
        public int In = 0;
        public int OutC = 0;
        public int OutVb = 0;
        public int DefaultVolume = 0;

        public static Profil ProfilBase()
        {
            return new Profil
            {
	            Playback = false,
	            LesSons = new List<Son>()
            };
        }

        public void Save()
        {
            using (var writer = new StreamWriter(Application.StartupPath + @"/Profil.cfg"))
            {
                writer.WriteLine(JsonConvert.SerializeObject(this));
            }
            
        }
        /*
        public void AddSon(Son leSon)
        {
            using (var a = new StreamWriter(Application.StartupPath + @"/Profil.cfg",true))
            {
                //a.WriteLine();
                a.WriteLine(leSon.Path+";"+leSon.Hotkey);
            }
            LesSons.Add(leSon);
        }

        public void ModifSon(Son leSon)
        {
            using (var reader = new StreamReader(Application.StartupPath + @"/Profil.cfg"))
            {
                using (var writer = new StreamWriter(Application.StartupPath + @"/Profil.cfg"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (string.Compare(line, leSon.Path + ";" + leSon.Hotkey) == 0)
                            writer.WriteLine(leSon.Path + ";" + leSon.Hotkey);
                        else
                            writer.WriteLine(line);
                    }
                }
            }
        }

        public void RemoveSon(Son leSon)
        {
            using (var reader = new StreamReader(Application.StartupPath + @"/Profil.cfg"))
            {
                using (var writer = new StreamWriter(Application.StartupPath + @"/Profil.cfg"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (string.Compare(line, leSon.Path+";"+leSon.Hotkey) == 0)
                            continue;

                        writer.WriteLine(line);
                    }
                }
            }
        }
        */
    }
}