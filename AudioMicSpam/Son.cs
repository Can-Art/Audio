﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace AudioMicSpam
{
    public class Son
    {
        public string Nom { get; set; }
        public string Path { get; set; }
        public Keys Hotkey { get; set; }

        public Son(string Path,Keys laKey = Keys.None)
        {
            var sdqs = new Uri(Path);
            Nom = sdqs.Segments.Last();
            this.Path = Path;
            Hotkey = laKey;
            Console.WriteLine("Nouvel objet crée Nom : " + Nom);  
        }

        public override string ToString()
        {
            return Nom;
        }
    }
}
