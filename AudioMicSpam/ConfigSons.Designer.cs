﻿namespace AudioMicSpam
{
    partial class ConfigSons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bsSons = new System.Windows.Forms.BindingSource(this.components);
            this.dgvSons = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.lbInfo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbOutVb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbOut = new System.Windows.Forms.ComboBox();
            this.cbIn = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.bsSons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSons)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvSons
            // 
            this.dgvSons.AutoGenerateColumns = false;
            this.dgvSons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSons.DataSource = this.bsSons;
            this.dgvSons.Location = new System.Drawing.Point(12, 12);
            this.dgvSons.Name = "dgvSons";
            this.dgvSons.Size = new System.Drawing.Size(438, 286);
            this.dgvSons.TabIndex = 0;
            this.dgvSons.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSons_CellClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(310, 338);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(13, 305);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(0, 13);
            this.lbInfo.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 361);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Out/VirtualCable :";
            // 
            // cbOutVb
            // 
            this.cbOutVb.FormattingEnabled = true;
            this.cbOutVb.Location = new System.Drawing.Point(144, 380);
            this.cbOutVb.Name = "cbOutVb";
            this.cbOutVb.Size = new System.Drawing.Size(121, 21);
            this.cbOutVb.TabIndex = 20;
            this.cbOutVb.SelectedIndexChanged += new System.EventHandler(this.cbOutVb_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "In/Mic :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 319);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Out/Speakers :";
            // 
            // cbOut
            // 
            this.cbOut.FormattingEnabled = true;
            this.cbOut.Location = new System.Drawing.Point(144, 338);
            this.cbOut.Name = "cbOut";
            this.cbOut.Size = new System.Drawing.Size(121, 21);
            this.cbOut.TabIndex = 17;
            this.cbOut.SelectedIndexChanged += new System.EventHandler(this.cbOut_SelectedIndexChanged);
            // 
            // cbIn
            // 
            this.cbIn.FormattingEnabled = true;
            this.cbIn.Location = new System.Drawing.Point(17, 338);
            this.cbIn.Name = "cbIn";
            this.cbIn.Size = new System.Drawing.Size(121, 21);
            this.cbIn.TabIndex = 16;
            this.cbIn.SelectedIndexChanged += new System.EventHandler(this.cbIn_SelectedIndexChanged);
            // 
            // ConfigSons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 402);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbOutVb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbOut);
            this.Controls.Add(this.cbIn);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvSons);
            this.KeyPreview = true;
            this.Name = "ConfigSons";
            this.Text = "ConfigSons";
            this.Load += new System.EventHandler(this.ConfigSons_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ConfigSons_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ConfigSons_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.bsSons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSons)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource bsSons;
        private System.Windows.Forms.DataGridView dgvSons;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbOutVb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbOut;
        private System.Windows.Forms.ComboBox cbIn;
    }
}